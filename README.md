# New Project

I use this to make a starter python project. It basically only creates 2 files at this time. `main.py` and `pyrightconfig.json` I want to configure more but this is a start. The `main.py` is just a basic layout of a python file to start with.  The `pyrightconfig.json` is used for sublime text/lsp pyright to tell what python environment your using in your project.

When you run this script it will create directory in the current directory with the name of the project that is specified. In that directory it will create a basic `main.py` and `pyrightconfig.json`.



## Prerequisites

- `Python 3.8` at a minimum - In Linux this should not be a problem most recent distros come with at least python 3.8. 


## Installing 

Installing python 3 on the raspberry pi or any Debian based Linux computer. Note any modern Linux system will already have python 3 installed.
```bash
sudo apt-get install python3 

```
Run this from a terminal in the directory where you want to clone the repository to your local computer

```bash
git clone https://gitlab.com/public_outlaws42/new_project.git


```
Change into the project main directory

```bash
cd new_project

```


You will have to make the python files executable.

```bash
chmod u+x *.py

```

## To run

**main.py**  This is the main file and is what you would run. 

To run the script

```bash
./main.py

```

**Note:** You should be able to run this code on Windows and MacOS as well as long as you have the 
prerequisites installed. Running the python files will be different for those platforms but it should be possible.

## License
GPL 2.0