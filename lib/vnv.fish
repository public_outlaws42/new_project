#!/usr/bin/env fish

# Fish shell create a new vf python virtual machine
# Then change into the environment 
# and update pip to latest if it is installed.

set revision "1.0.4"


function read_install
  # prompt if you want to install
  while true
    read -l confirm -P 'Do you want to install? [Y/N]: '

    switch $confirm
    case Y y
      return 0
    case '' N n
      return 1
    end
  end
end

function check_argv
  # check if argument exists if not it exits out of the script
  if not set -q $argv[1]
    return 0
  else
    echo "Please provide a name for the virtual environment"
    echo "Nothing was created"
    exit 
  end
end

function check_exists
  # check if dir exists
  set vf (echo "$HOME/.virtualenvs/$argv[1]")
  if [ -d "$vf" ]
    echo "The virtual environment $argv[1] apears to exist already"
    exit
  else
    return 0
  end

end

function new_env
  # Create a vf in fish
  check_argv $argv[1]
  vf new $argv[1]
  echo "virtual Environment $argv[1] was created"
end

function activate_env
  # Activate vf
  check_argv $argv[1]
  vf activate $argv[1]
  echo "$argv[1] virtual Environment is activated"
end

function update_pip
  # Upgrade pip
  set com (which pip3)
  if [ -n "$com" ] 
    $com install --upgrade pip
  else
    echo "pip is not installed"

    if read_install
      # ubuntu/debian install pip
      sudo apt install python3-pip
    end
  end
end


# Check to make sure environment name provided 
check_argv $argv

# check if it already exists
check_exists $argv

# if checks pass then create env
new_env $argv
activate_env $argv
update_pip

