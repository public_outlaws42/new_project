#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
input logic for user input
"""
# Imports

# Personal Imports
from lib.colors import Colors

c = Colors()

__author__ = "Troy Franks"
__version__ = "2023-06-01"


def input_single(
    in_message: str,
    in_type: str = "email",
    fdest: str = "home",
    max_number: int = 200,
    default: str = "",
    # req: str = None,
):
    """
    in_message = the message you want in your input string,\n
    in_type = the type of input field. Choices are\n
      'email' - This will expect a email address\n
      'file' - this will expect a valid file at location specified\n
      'number' - This will expect a number\n
      'time12' - this will expect a 12 hour time (HH:MM am or pm)\n
      'time24' - This will expect a 24 hour time (HH:MM)\n
      'ip4' - This will expect a ipv4 ip\n
      'ip6' - This will expect a ipv6 ip\n
      'zip5' - This will expect a 5 digit US zip code\n
      'zip9' - This will expect a 9 digit US zip code\n
    max_number = if a number input the max number you can enter
    default = Add a value if the user dosen't input anything
    This is for a single item input. This uses "validate_input"
    to verify that items entered meet requirements for that type of input
    """
    if in_type == "number":
        item = (
            input(
                (
                    f"{c.BOLD}{in_message}{c.GREEN}(max {max_number}) "
                    f"(Default: {default}): {c.END}"
                )
            )
            or default
        )
    else:
        item = (
            input(
                (
                    f"{c.BOLD}{in_message} {c.GREEN}(Default: "
                    f"{default}): {c.END}"
                )
            )
            or default
        )
    while (
        validate_input(
            item=item,
            in_type=in_type,
            fdest=fdest,
            max_number=max_number,
        )
        is False
    ):
        err_message = error_single(in_type=in_type)
        print((f"{c.RED}{c.BOLD}" f"{err_message}{c.END}\n"))
        if in_type == "int" or in_type == "float":
            item = (
                input(
                    (
                        f"{c.BOLD}{in_message}{c.GREEN}(max {max_number}) "
                        f"(Default: {default}): {c.END}"
                    )
                )
                or default
            )
        else:
            item = (
                input(
                    (
                        f"{c.BOLD}{in_message} {c.GREEN}(Default: "
                        f"{default}): {c.END}"
                    )
                )
                or default
            )

    print(f"\n{c.BOLD}You entered {c.CYAN}{item}{c.END}\n")
    if in_type == "int":
        return int(item)
    elif in_type == "float":
        return float(item)
    else:
        return item


def input_choice(
    in_message: str,
    choices: dict = {1: "option 1", 2: "option 2"},
    default_choice: int = 1,
):
    """
    in_message = the message you want in your input string,\n
    choices = dictionary of options for your choices.
    The key being a int the user will select. The value
    being the option used\n
    default_choice = This is the choice that will be
    selected if the user doesn't choose any option.

    """

    print(f"\n{c.BLUE}{c.BOLD}Select a item from the following items{c.END}")
    for key, value in choices.items():
        print(f"{c.BOLD}{key} = {c.YELLOW}{value}{c.END}")
    try:
        item = (
            input(
                f"{c.BOLD}{in_message} {c.GREEN}(default {default_choice}):{c.END} "
            )
            or default_choice
        )
        item = int(item)
        error_choice_keys(item=item, choices=choices)
    except ValueError:
        print((f"{c.RED}{c.BOLD}" f"Select a valid option{c.END}\n"))
        item = 1000
    while item not in choices.keys():
        try:
            item = (
                input(
                    f"{c.BOLD}{in_message} {c.GREEN}(default {default_choice}):{c.END} "
                )
                or default_choice
            )
            item = int(item)
            error_choice_keys(item=item, choices=choices)
        except ValueError:
            # Output error if item something other than a number
            error_choice_except()
            item = 1000
    choice = choices[item]
    print(f"{c.BOLD}You chose {c.YELLOW}{choice}{c.END}")
    return choice


# Error messages


def error_single(in_type: str):
    """
    Error message for the input single
    """
    if in_type == "no_space":
        return "Word can't have any spaces"
    else:
        return f"This is not a valid {in_type}"


def error_choice_keys(
    item: int,
    choices: dict,
):
    """
    Output error message if index not in the list
    """
    if item not in choices.keys():
        print((f"{c.RED}{c.BOLD}" f"Please select a item in the list{c.END}\n"))


def error_choice_except():
    """
    Output error if item something other than a number
    """
    print((f"{c.RED}{c.BOLD}" f"Select a valid option{c.END}\n"))


def validate_input(
    item,
    in_type: str,
    fdest: str = "home",
    max_number: int = 200,
):
    """
    item = The data entered in the input field,
    in_type = The type of data it is suposed to be,
    email address\n
    ip address = 'ip4' or 'ip6'\n

    max_number = the max number that can be applied this is for int only.
    Takes the input and checks to see if it is
    valid for its data type.
    """
    if in_type == "no_space":
        return validate_no_space_string(item=item)
    elif in_type == "ip4" or in_type == "ip6":
        pass
        # return vi.validate_ip(item=item, ip_type=in_type)
    elif in_type == "zip5" or in_type == "zip9":
        pass
        # return vi.validate_zip(item=item, zip_type=in_type)
    elif in_type == "file":
        pass
        # return vi.validate_file(item=item, fdest=fdest)
    elif in_type == "password":
        pass
        # return vi.validate_password(item=item)
    elif in_type == "time24" or in_type == "time12":
        pass
        # return vi.validate_time(item=item, time_type=in_type)
    elif in_type == "number":
        pass
        # return vi.validate_num(item=item, max_number=max_number)
    elif in_type == "string":
        return True
    else:
        return False


def validate_no_space_string(item: str):
    if " " not in item:
        return True
    else:
        return False


if __name__ == "__main__":
    pass
