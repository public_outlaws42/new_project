#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
input wizard library for setting up a new python project
"""
# Imports
from datetime import date

# Personal Imports
import lib.inpt as inpt

__author__ = "Troy Franks"
__version__ = "2023-07-30"


def get_venv_name():
    message = "Virtual environment name (No spaces)"
    name = inpt.input_single(
        in_message=message,
        in_type="no_space",
        default="mainenv",
    )
    return str(name)


def get_project_name():
    message = "What do you want to call this project (No spaces)"
    name = inpt.input_single(
        in_message=message,
        in_type="no_space",
        default="myProject",
    )
    return str(name)


def get_path():
    message = (
        "Where do want the new project relative to your "
        "home directory(example: Projects/Python_Projects)"
    )
    name = inpt.input_single(
        in_message=message,
        in_type="no_space",
        default="Projects/Python_Projects",
    )
    return str(name)


def get_author_name():
    message = "What is the name of the programmer"
    name = inpt.input_single(
        in_message=message,
        in_type="string",
        default="outlaws42",
    )
    return str(name)


def get_vf_answer():
    message = "Do you want to create a virtual fish environment"
    name = inpt.input_single(
        in_message=message,
        in_type="string",
        default="no",
    )
    return str(name)


def get_location():
    message = (
        "Do You want your new project directory relative"
        "to your home directory or your current directory (CD)"
    )
    name = inpt.input_choice(
        in_message=message,
        choices={1: "Home", 2: "CD"},
        default_choice=1,
    )
    return str(name)


def get_version():
    message = "What is the starting version"
    name = inpt.input_single(
        in_message=message,
        in_type="string",
        default=f"{date.today()}",
    )
    return str(name)


if __name__ == "__main__":
    pass
