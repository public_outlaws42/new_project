#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Library to give file location and infromation
"""
# Imports
from json import dump
from pathlib import Path


__author__ = "Troy Franks"
__version__ = "2023-07-31"


def home_dir():
    return Path.home()


def get_resource_path(rel_path):
    """
    returns full path of the relative path provided
    requires the pathlib module
    """
    dir_of_py_file = Path(__file__).parent.resolve()
    abs_path_to_resource = dir_of_py_file.joinpath(rel_path)
    return abs_path_to_resource


def check_file_dir(
    fname: str,
    fdest: str = "home",
    path: str = "Project/Python_Projects",
):
    """
    fname = name of the file or folder,
    fdest = the destination, either home or relative to CWD
    Check if file or folder exist returns True or False
    Requires import os
    """
    home = home_dir()
    if fdest.lower() == "home":
        fpath = f"{home}/{path}/{fname}"
    else:
        fpath = get_resource_path(fname)
    file_exist = Path.exists(Path(fpath))
    return file_exist


def mkdir_location(
    fname: str,
    fdest: str = "home",
    path: str = "Project/Python_Projects",
):
    home = home_dir()

    if fdest.lower() == "home":
        Path(home / path / fname).mkdir(parents=True, exist_ok=False)
    else:
        Path(get_resource_path(fname)).mkdir(parents=True, exist_ok=False)


def save_file(
    fname: str,
    content: str,
    fdest: str = "relative",
    mode: str = "w",
    path: str = "Project/Python_Projects",
):
    """
    fname = filename, content = what to save to the file,
    fdest = where to save file, mode = w for write or a for append
    """
    home = home_dir()
    if fdest.lower() == "home":
        with open(f"{home}/{path}/{fname}", mode) as output:
            output.write(content)
    else:
        with open(get_resource_path(fname), mode) as output:
            output.write(content)


def save_json(
    fname: str,
    content: dict,
    path: str,
    fdest: str = "relative",
):
    home = home_dir()
    if fdest == "home" or fdest == "Home":
        with open(f"{home}/{path}/{fname}", "w") as output:
            dump(content, output, sort_keys=True, indent=4)
    else:
        with open(get_resource_path(fname), "w") as output:
            dump(content, output, sort_keys=True, indent=4)


if __name__ == "__main__":
    pass
