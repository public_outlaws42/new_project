#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
creates starter python project
"""
# Imports
import subprocess

# from json import dump

# Personal Imports
import lib.wizard as wi
import lib.file_info as fi

__author__ = "Troy Franks"
__version__ = "2023-07-31"

# Global Variables
home = fi.home_dir()


def create_project_dir(
    project_name: str,
    location: str,
    path: str,
):
    exists = fi.check_file_dir(
        fname=project_name,
        fdest=location,
        path=path,
    )
    if exists is False:
        process = fi.mkdir_location(
            fname=project_name,
            fdest=location,
            path=path,
        )
        print(process)
    else:
        print(f"{project_name} already exists")


def create_pyright_config(
    project_name: str,
    venv: str,
    path: str,
    location: str,
    venv_path: str = f"{home}/.virtualenvs",
):
    """
    Creates a pyrightconfig.json file in the root of the project to specify
    what virtual environment is being used. This is for the lisp pyright
    code checking and completion in sublime text.
    """
    file_name = "pyrightconfig.json"
    exists = fi.check_file_dir(
        fname=f"{project_name}/{file_name}",
        fdest=location,
    )

    content = {}
    content["venv"] = venv
    content["venvPath"] = venv_path

    if exists is False:
        fi.save_json(
            fname=f"{project_name}/{file_name}",
            content=content,
            fdest=location,
            path=path,
        )
        print(f"{project_name}/{file_name} was created")
    else:
        print(f"{project_name}/{file_name} already exists")


def create_py_file(
    project_name: str,
    programer: str,
    version: str,
    location: str,
    path: str,
    file_name: str = "main.py",
):
    exists = fi.check_file_dir(
        fname=f"{project_name}/{file_name}",
        fdest=location,
        path=path,
    )

    content = (
        f"#!/bin/bash/env python3\n# -*- coding: utf-8 -*-"
        f'\n\n"""\nAdd project comments here\n"""'
        f"\n\n# Imports\nfrom os import path\n\n# Personal Imports\n\n"
        f'__author__ = "{programer}"\n__version__ = "{version}"\n\n'
        f'# Global Variables\nhome = path.expanduser("~")\n\n'
        f'if __name__ == "__main__":\n    pass\n'
    )
    if exists is False:
        fi.save_file(
            fname=f"{project_name}/{file_name}",
            content=content,
            fdest=location,
            path=path,
        )
        print(f"{project_name}/{file_name} was created")
    else:
        print(f"{project_name}/{file_name} already exists")


def create_vf(
    create_vf: str,
    venv: str,
):
    if create_vf.lower() == "yes":
        process = subprocess.Popen(
            ["fish"],
            stdout=subprocess.PIPE,
            text=True,
        )
        result = process.communicate

        make_vf = subprocess.run(
            [f'{fi.get_resource_path("lib/vnv.fish")}', f"{venv}"],
            capture_output=True,
            text=True,
        )
        print(make_vf.stdout)
    else:
        print(f"No virtual environment was created")


if __name__ == "__main__":
    location = wi.get_location()
    path = ""
    if location.lower() == "home":
        path = wi.get_path()
    project_name = wi.get_project_name()
    venv = wi.get_venv_name()

    programer = wi.get_author_name()
    start_version = wi.get_version()
    vf = wi.get_vf_answer()

    create_project_dir(
        project_name=project_name,
        location=location,
        path=path,
    )
    create_pyright_config(
        project_name=project_name,
        venv=venv,
        location=location,
        path=path,
    )
    create_py_file(
        project_name=project_name,
        programer=programer,
        version=start_version,
        location=location,
        file_name="main.py",
        path=path,
    )

    create_vf(
        create_vf=vf,
        venv=venv,
    )
